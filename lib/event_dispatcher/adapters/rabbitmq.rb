require 'bunny'
require 'ostruct'
require 'json'

module EventDispatcher
  module Adapters
    class RabbitMQ
      def initialize(options = {})
        connection = create_connection(options)
        @channel = connection.create_channel
        @exchanges = {}
        @queues = {}
        @mutex = Mutex.new
      end

      def subscribe(event_name, listener_id, &block)
        with_mutex do
          queue = queue_for(listener_id)
          queue.bind(exchange_for(event_name))
          queue.subscribe(:manual_ack => true) do |delivery_info, properties, payload|
            event_object = OpenStruct.new(JSON.parse(payload))
            block.call(event_object)
            @channel.ack(delivery_info.delivery_tag)
          end
        end
      end

      def publish(event_name, event_hash)
        with_mutex do
          exchange = exchange_for(event_name)
          exchange.publish(event_hash.to_json)
        end
      end

      private
      def create_connection(options)
        connection = ::Bunny.new(options)
        connection.start
        at_exit { connection.close }
        connection
      end

      def queue_for(listener_id)
        @queues[listener_id] ||= @channel.queue(listener_id)
      end

      def exchange_for(event_name)
        @exchanges[event_name] ||= @channel.fanout(event_name)
      end

      def with_mutex(&block)
        @mutex.synchronize(&block)
      end
    end
  end
end
