require 'ostruct'

module EventDispatcher
  module Adapters
    class InMemory
      def initialize
        @listeners = Hash.new { |h, k| h[k] = [] }
        @mutex = Mutex.new
      end

      def subscribe(event_name, _, &block)
        with_mutex do
          @listeners[event_name] << block
        end
      end

      def publish(event_name, event_hash)
        event_object = OpenStruct.new(event_hash)
        with_mutex do
          @listeners[event_name].each do |listener|
            listener.call(event_object)
          end
        end
      end

      private
      def with_mutex(&block)
        @mutex.synchronize(&block)
      end
    end
  end
end
