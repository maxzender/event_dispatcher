module EventDispatcher
  module Subscriber
    def self.included(klass)
      klass.extend(self)
    end

    def subscribe(event_name, &listener)
      EventDispatcher.subscribe(event_name, self.name, &listener)
    end
  end
end
