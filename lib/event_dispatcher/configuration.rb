module EventDispatcher
  class Configuration
    attr_accessor :adapter

    def initialize
      @adapter = EventDispatcher::Adapters::InMemory.new
    end

    def self.instance
      @configuration ||= Configuration.new
    end

    def self.configure
      yield instance
    end

    def self.reset
      @configuration = nil
    end
  end
end
