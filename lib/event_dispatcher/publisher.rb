module EventDispatcher
  module Publisher
    def publish(event_name, event_hash)
      EventDispatcher.publish(event_name, event_hash)
    end
  end
end
