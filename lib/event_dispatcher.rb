require 'event_dispatcher/configuration'
require 'event_dispatcher/subscriber'
require 'event_dispatcher/publisher'
require 'event_dispatcher/adapters/in_memory'
require 'event_dispatcher/adapters/rabbitmq'

module EventDispatcher
  def self.config
    Configuration.instance
  end

  def self.configure(&block)
    Configuration.configure(&block)
  end

  def self.adapter
    config.adapter
  end

  def self.publish(event_name, event_hash)
    adapter.publish(event_name, event_hash)
  end

  def self.subscribe(event_name, listener_id, &listener)
    adapter.subscribe(event_name, listener_id, &listener)
  end
end
