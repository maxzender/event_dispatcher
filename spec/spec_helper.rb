require_relative '../lib/event_dispatcher.rb'
require_relative 'helpers/helper.rb'

RSpec.configure do |config|
  config.backtrace_exclusion_patterns = [/rspec-core/]

  # Use color in STDOUT
  config.color = true

  # Use color not only in STDOUT but also in pagers and files
  config.tty = true

  config.include Helper
end
