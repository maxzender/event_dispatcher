require "spec_helper"

describe EventDispatcher::Adapters::RabbitMQ do
  let(:listener1) { Proc.new {} }
  let(:listener2) { Proc.new {} }
  let(:event_hash) { { :foo => "bar" } }
  let(:event_object) { OpenStruct.new(event_hash) }

  after do
    delete_exchanges :event_one
    delete_queues :listener1, :listener2
  end

  it "should publish an event to an asynchronous subscriber" do
    subject.subscribe(:event_one, :listener1, &listener1)

    expect(listener1).to receive(:call).with(event_object)

    subject.publish(:event_one, event_hash)
    sleep 0.7 # This is the same interval that bunny uses for testing purposes.
  end

  it "should publish multiple events to a subscribers" do
    subject.subscribe(:event_one, :listener1, &listener1)
    subject.subscribe(:event_one, :listener2, &listener2)

    expect(listener1).to receive(:call).with(event_object)
    expect(listener2).to receive(:call).with(event_object)

    subject.publish(:event_one, event_hash)
    sleep 0.7 # This is the same interval that bunny uses for testing purposes.
  end
end
