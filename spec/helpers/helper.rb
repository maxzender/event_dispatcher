require 'bunny'

module Helper
  def delete_queues(*queues)
    bunny = Bunny.new
    bunny.start
    ch = bunny.create_channel
    queues.each do |q|
      ch.queue_delete(q.to_s)
    end
    bunny.close
  end

  def delete_exchanges(*exchanges)
    bunny = Bunny.new
    bunny.start
    ch = bunny.create_channel
    exchanges.each do |exchange|
      ch.exchange_delete(exchange.to_s)
    end
    bunny.close
  end
end
