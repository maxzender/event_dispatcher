require "spec_helper"

describe EventDispatcher do
  describe "#configure" do
    before do
      EventDispatcher.configure do |config|
        config.adapter = :foo
      end
    end

    after { EventDispatcher::Configuration.reset }

    let(:config) { EventDispatcher.config }

    it "memorises the configured values" do
      expect(config.adapter).to eql(:foo)
    end
  end
end
