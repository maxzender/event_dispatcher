require "spec_helper"

describe EventDispatcher::Subscriber do
  let(:handler) { Proc.new {} }

  class SubscriberClass
    include EventDispatcher::Subscriber
  end

  subject { SubscriberClass.new }

  it "calls EventDispatcher.subscribe with the correct arguments" do
    expect(EventDispatcher).to receive(:subscribe).with(:event_one, 'SubscriberClass', &handler)
    subject.class.subscribe(:event_one, &handler)
  end

end
