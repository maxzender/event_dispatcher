require "spec_helper"

describe EventDispatcher::Publisher do
  class PublisherClass
    include EventDispatcher::Publisher
  end

  subject { PublisherClass.new }

  it "calls EventDispatcher.publish with the correct arguments" do
    expect(EventDispatcher).to receive(:publish).with(:event_one, :foo => "bar")
    subject.publish(:event_one, :foo => "bar")
  end

end
