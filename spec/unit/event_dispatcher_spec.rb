require "spec_helper"
require "ostruct"

describe EventDispatcher do

  let(:listener1) { Proc.new {} }
  let(:listener2) { Proc.new {} }
  let(:event_hash) { { :foo => "bar" } }
  let(:event_object) { OpenStruct.new(event_hash) }

  it "should publish an event to its subscriber" do
    subject.subscribe(:event_one, :listener1, &listener1)
    expect(listener1).to receive(:call).with(event_object)
    subject.publish(:event_one, event_hash)
  end

  it "should publish multiple events to its subscribers" do
    subject.subscribe(:event_one, :listener1, &listener1)
    subject.subscribe(:event_one, :listener2, &listener2)

    expect(listener1).to receive(:call).with(event_object)
    expect(listener2).to receive(:call).with(event_object)

    subject.publish(:event_one, event_hash)
  end

  it "should publish events only to their respective subscribers" do
    subject.subscribe(:event_one, :listener1, &listener1)
    subject.subscribe(:event_two, :listener2, &listener2)

    expect(listener1).to receive(:call).with(event_object)
    expect(listener2).to_not receive(:call)

    subject.publish(:event_one, event_hash)
  end
end
