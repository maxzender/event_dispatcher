lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |s|
  s.name          = 'event_dispatcher'
  s.version       = '0.0.1'
  s.date          = '2015-09-08'
  s.summary       = "Dispatches events to RabbitMQ and provides a convenient DSL for publishing and subscribing to them."
  s.description   = "Dispatches events to RabbitMQ and provides a convenient DSL for publishing and subscribing to them."
  s.authors       = ["Max Zender"]
  s.email         = 'mail@maxzender.de'
  s.require_paths = ['lib']
  s.license       = 'MIT'

  s.executables   = s.files.grep(%r{^bin/}) { |f| File.basename(f) }
  s.test_files    = s.files.grep(%r{^(test|spec|features)/})
end
